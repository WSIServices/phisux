<?php

use \WSIServices\Phisux;

return array(
	'vendorDirectory' => realpath('../../../../vendor'),
	'environmentDirectory' => realpath('../../../../source/core/env'),
	'temporaryDirectory' => realpath('../../../../source/core/tmp'),

	// Start Kernel Section: Initialization of Kernel and system
	'start_kernel' => array(
		// Eager Load Section: array of class files
		// Include class files prior to Autoload comming online
		'eagerLoad' => array(
		),
		'modulesHandler' => array(
			'_construct' => function(&$config) { return new Phisux\ModulesStub($config); },
		),
		// Initialization Section: array of closures
		// Closure provided contents of this config array as first paramiter
		'init' => array(
		),
	),
);
