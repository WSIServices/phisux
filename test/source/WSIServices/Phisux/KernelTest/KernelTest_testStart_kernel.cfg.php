<?php

use \WSIServices\Phisux;

return array(
	'coreDirectory' => 'coreDirectory',
	'vendorDirectory' => 'vendorDirectory',
	'environmentDirectory' => 'environmentDirectory',
	'temporaryDirectory' => 'temporaryDirectory',

	// Start Kernel Section: Initialization of Kernel and system
	'start_kernel' => array(
		'modulesHandler' => array(
			'_construct' => function(&$config) { return new Phisux\ModulesStub($config); },
		),
	),
);
