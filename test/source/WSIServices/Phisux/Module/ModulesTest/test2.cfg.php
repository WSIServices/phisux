<?php

use \WSIServices\Phisux\Module\Modules;

return array(
	'_construct' => function(&$config) { return new Modules($config); },
	'test' => true,
	'value' => 'Test Modules',
	'module' => 'ModulesTest2',
);