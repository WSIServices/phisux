<?php

ob_start(); // Catch content for late header manipulation

include_once('PHPUnit/Autoload.php');

include_once(__DIR__.'/../vendor/autoload.php');
