README
======

Phisux PHP Kernel
-----------------

Phisux provides a set of components to assist with building PHP applications.  Focusing on an API driven architecture, Phisux is a non-MVC style design.

Requirements
------------

Requirements have not been tested.

Developed on PHP 5.3.2

Installation
------------

Cloning the projects git repository or downloading from the [repository project page][0] can provide you with the most up-to-date code and is currently the only methods for obtaining the package.

	git clone https://bitbucket.org/WSIServices/phisux.git

Documentation
-------------

Documentation is not available at this time, however the code is commented in PhpDocumentor format and as time allows documentation will be generated.

[0]: http://bitbucket.wsi-services.com/phisux
