<?php

namespace WSIServices\Phisux;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * http://en.wikipedia.org/wiki/Linux_startup_process#Kernel_startup_stage
 * http://www.ibm.com/developerworks/linux/library/l-linuxboot/
 * http://www.thegeekstuff.com/2011/02/linux-boot-process/
 * http://www.yolinux.com/TUTORIALS/LinuxTutorialInitProcess.html
 * http://en.wikipedia.org/wiki/Linux_startup_process#Kernel_startup_stage
 * http://manpages.ubuntu.com/manpages/lucid/man7/boot.7.html
 */
class Kernel {

	/**
	 * Kernel class version
	 */
	CONST VERSION = '0.1.1';

	/**
	 * @var string Path to configuration
	 */
	static protected $config_path;

	/**
	 * @var object Set AutoLoader
	 */
	static protected $autoLoader;

	/**
	 * @var callback Callback for the AutoLoader
	 */
	static protected $autoLoaderCallback;

	/**
	 * @var object Kernel singelton instance
	 */
	static protected $kernel;

	/**
	 * @var array Kernel start configuration
	 */
	static protected $start_kernel = false;

	/**
	 * @var array Kernel modules
	 */
	static protected $modules;

	/**
	 * @var string Location of core directory
	 */
	static public $coreDirectory;

	/**
	 * @var string Location of vendor directory
	 */
	static public $vendorDirectory;

	/**
	 * @var string Location of namespace directory
	 */
	static public $environmentDirectory;

	/**
	 * @var string Location of temporary directory
	 */
	static public $temporaryDirectory;

	/**
	 *
	 * @param string $path Path to kernel configuration
	 * @return string Path to kernel configuration
	 * @throws \RuntimeException If kernel_start() has been executed
	 * @throws \UnexpectedValueException If provided path is not valid or readable
	 */
	static public function configPath($path = null) {
		if($path === null) return static::$config_path;
		if(static::$start_kernel !== false) throw new \RuntimeException('Kernel configuration path can not be set after start_kernel() is run');
		if(!is_readable($path)) throw new \UnexpectedValueException('Provided kernel configuration path is not readable');
		static::$config_path = $path;
	}

	/**
	 * Set Class loading callback for usage in earger loading
	 * @param callback $callback Callable method for loading classes or null to clear
	 * @deprecated Use setAutoLoader or clearAutoLoader
	 */
	static public function classLoadCallback($callback) {
		if($callback)
			static::setAutoLoader($callback);
		else
			static::clearAutoLoader();
	}

	/**
	 * Set AutoLoader
	 * @param mixed $autoLoader Object or Callback
	 * @param mixed $method String method name of Object to use as callable to load class files
	 * @return boolean True if set, false otherwise
	 */
	static public function setAutoLoader($autoLoader, $method = null) {
		if(is_object($autoLoader)) {
			static::$autoLoader = $autoLoader;
			if($method !== null) {
				if(!method_exists($autoLoader, $method)) return false;
				static::$autoLoaderCallback = array($autoLoader, $method);
			}
		} elseif(is_callable($autoLoader)) {
			static::$autoLoaderCallback = $autoLoader;
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Call AutoLoader callable 
	 * @param mixed $classes String class name or array of class names
	 * @return boolean Return value from AutoLoader callable
	 */
	static public function callAutoLoader($classes) {
		if(!static::$autoLoaderCallback) return false;
		if(is_string($classes))
			return call_user_func(static::$autoLoaderCallback, $classes);
		if(is_array($classes))
			foreach($classes as $class)
				call_user_func(static::$autoLoaderCallback, $class);
				
	}

	/**
	 * Return the AutoLoader object
	 * @return object AutoLoader object
	 */
	static public function getAutoLoader() {
		return static::$autoLoader;
	}

	/**
	 * Clear the AutoLoader and Callable
	 */
	static public function clearAutoLoader() {
		static::$autoLoader = null;
		static::$autoLoaderCallback = null;
	}

	/**
	 * Static kernel trigger to initialize kernel configuration
	 * @param mixed $configuration Configuration for kernel provided as file
	 * location string or array configuration.  Providing array, 
	 * @throws \UnexpectedValueException 
	 */
	static public function start_kernel(&$configuration = null) {

		if(is_string($configuration) && $configuration) {
			// Configuration path provided as string
			static::configPath($configuration);
			$configuration = array();
		} elseif(is_array($configuration)) {
			// Configuration path provided in array
			if(key_exists('config_path', $configuration) && is_readable($configuration['config_path'])) {
				static::$config_path = $configuration['config_path'];
				unset($configuration['config_path']);
			}

			// Set start_kernel for 
			if(static::$start_kernel && key_exists('_restart_kernel', $configuration)) {
				if($configuration['_restart_kernel'] === true) static::$start_kernel = false;
				unset($configuration['_restart_kernel']);
			}
		} else {
			$configuration = array();
		}

		// Run if method has not been called previously
		if(static::$start_kernel === false) {

			if(static::$config_path) {
				// Configuration path provided load & overwrite by provided array
				$configuration = array_replace_recursive(include(static::$config_path), $configuration);
			}

			// Must be an array
			if(!is_array($configuration)) throw new \UnexpectedValueException('Kernel Configuration is not a valid array');

			// Class paramiters
			$classVariables = get_class_vars(__CLASS__);

			// Get class paramiters that are not configuration keys
			$missingParams = array_diff_key($classVariables, $configuration);

			// Create configuration keys and store class paramiter values
			foreach($missingParams as $classParamiter => $paramiterValue)
				$configuration[$classParamiter] = $paramiterValue;

			// Get configuration keys that are also class paramiters
			$classUpdate = array_intersect_key($configuration, $classVariables);

			// Overwrite Kernel paramiters with refferanced configuration
			foreach($classUpdate as $classParamiter => $paramiterValue)
				static::$$classParamiter =& $configuration[$classParamiter];

			// Run through start kernel processes
			if(is_array(static::$start_kernel)) {

				// Include EagerLoad list
				if(key_exists('eagerLoad', static::$start_kernel) && is_array(static::$start_kernel['eagerLoad']))
					static::callAutoLoader(static::$start_kernel['eagerLoad']);

				// Load Module Handler
				if(key_exists('modulesHandler', static::$start_kernel) && key_exists('_construct', static::$start_kernel['modulesHandler'])) {
					$construct = static::$start_kernel['modulesHandler']['_construct'];
					static::$modules = $construct(static::$start_kernel['modulesHandler']);
					unset($construct);
				}

				// Run through start kernel processes
				if(key_exists('init', static::$start_kernel) && is_array(static::$start_kernel['init']))
					foreach(static::$start_kernel['init'] as &$closure) $closure($config);

			}

		}

		static::$start_kernel = true;
	}

	/**
	 * Invoke specified module from kernel
	 * @param string $name Name of module
	 * @param array $arguments Arguments passed to module
	 * @return mixed
	 */
	static public function __callStatic($name, $arguments) {
		// Retrieve requested module
		if(!is_object(static::$modules)) throw new \RuntimeException('Kernel Module Handler is not loaded');
		elseif(!method_exists(static::$modules, 'module')) throw new \LogicException('Kernel Module Handler missing module method');
		else $module = static::$modules->module($name);

		// If module is invokeable call module with paramiters & return results
		if(method_exists($module, '__invoke'))
			return call_user_func_array(array($module, '__invoke'), $arguments);

		// Return module
		return $module;
	}

	/**
	 * Get instance of Kernel
	 * @return Kernel
	 */
	static public function getInstance() {
		if(static::$kernel === null) static::$kernel = new Kernel();
		return static::$kernel;
	}

	/**
	 * Reset instance of Kernel 
	 */
	static public function resetInstance() {
		static::$kernel = null;
	}

	/**
	 * Invoke specified module from kernel
	 * @param string $name Name of module 
	 * @param array $arguments Array of arguments passed to module
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		return static::__callStatic($name, $arguments);
	}
}