<?php

namespace WSIServices\Phisux\Module;
use \WSIServices\Common\DirectoryIncluder;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class ClosureHelper extends DirectoryIncluder {

	public $extension = '.fn.php';

	protected $closures = array();

	public function __construct(&$configuration = null) {
		parent::__construct($configuration);

		// Eager load modules
		if(key_exists('eagerLoad', $configuration)) $this->load($configuration['eagerLoad']);
	}

	/**
	 * Check if closure is loadable
	 * @param string $name Name of closure to check
	 * @param boolean $returnPath Return path of module if loadable
	 * @return mixed If $returnPath is true, return string of closure path, else return boolean
	 */
	public function loadable($name, $returnPath = false) {
		return parent::loadable(strtolower($name), $returnPath);
	}

	/**
	 * Check if closure is loaded
	 * @param string $name Name of closure to check
	 * @return boolean true if closure is loaded, false otherwise 
	 */
	public function loaded($name) {
		return key_exists($name, $this->closures);
	}

	/**
	 * Check if closure is loaded or loadable
	 * @param string $name Name of closure to check
	 * @return boolean true if closure is loaded or loadable, false otherwise
	 */
	public function exists($name) {
		if($this->loaded($name)) return true;
		return $this->loadable($name);
	}

	/**
	 * Load specified closure
	 * @param string $name Name of closure
	 * @throws \BadMethodCallException
	 * @throws \InvalidArgumentException 
	 */
	public function load($name) {
		if(is_array($name)) foreach($name as $closureLoad) $this->load($closureLoad);
		elseif(is_string($name)) {
			try{
				$closure = parent::load($name);
			} catch(UnexpectedValueException $e) {
				throw new \BadMethodCallException("Closure '$name' is not available");
			}

			if(!is_object($closure) || !($closure instanceof \Closure)) throw new \InvalidArgumentException("$name is not a closure");
			$this->closures[$name] = $closure;
		}
	}

	/**
	 * Retrieve specified closure
	 * @param string $name Name of closure to return
	 * @return mixed Requested closure
	 */
	public function closure($name) {
		// Load closure if missing
		if(!key_exists($name, $this->closures)) $this->load($name);

		// Return Module
		return $this->closures[$name];
	}

	/**
	 * Invoke specified module
	 * @param string $name Name of closure
	 * @param array $arguments Arguments passed to module
	 * @return mixed Module or returned value from module __invode if exists 
	 */
	public function __call($name, $arguments) {

		$closure = $this->closure($name);

		return call_user_func_array(array($closure, '__invoke'), $arguments);
	}

}