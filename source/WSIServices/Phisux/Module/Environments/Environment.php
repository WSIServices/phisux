<?php

namespace WSIServices\Phisux\Module\Environments;
use WSIServices\Phisux\Module\Environments;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class Environment extends Environments {

	/**
	 * Provide configuration
	 * @param mixed $configuration [optional]
	 *
	 * @example Providing No Variable
	 *	$environment = new Environment();
	 *
	 * @example Providing Empty Variable
	 *	$environment = new Environment($configuration);
	 *
	 * @example Providing Empty Variable
	 *	$configuration = __DIR__;
	 *	$environment = new Environment($configuration);
	 *
	 * @example Providing Array
	 *	$configuration = array(
	 *		'directory' => __DIR__,
	 *		'extension' => '.php',
	 *	);
	 *	$environments = new Environments($configuration);
	 *
	 */
	public function __construct(&$configuration = null) {
		parent::__construct($configuration);
	}

	/**
	 * Get path of file
	 * @param string $name Name of file to return path
	 * @return string Path to specified file, false if the file does not exist.
	 *
	 * @example
	 * $path = $environment->path('filename');
	 */
	public function path($name) {
		// Only run path if directory is provided
		if(!$this->directory) return false;
		return parent::path($name);
	}

	/**
	 * Determin if specified environment component exists
	 * @param string $name Name of environment component
	 * @return boolean
	 */
	public function __isset($name) {
		return $this->exists($name);
	}

	/**
	 * Return specified environment component
	 * @param string $name Name of environment component
	 * @return mixed 
	 */
	public function &__get($name) {
		return $this->environments[$name];
	}

	/**
	 * Set specified environment component
	 * @param string $name Name of environment component
	 * @param mixed $value Value to store for environment component
	 */
	public function __set($name, $value) {
		$this->environments[$name] = $value;
	}

	/**
	 * Unset specified environment component
	 * @param string $name Name of environment component
	 */
	public function __unset($name) {
		unset($this->environments[$name]);
	}

	/**
	 * Retrieve Node section specified
	 * @param mixed $name Namespace style string or array
	 * @return mixed
	 * @throws \UnexpectedValueException 
	 */
	public function &node($name) {
		// Expand each namespace into array
		if(is_string($name)) $name = explode('\\', $name);
		if(!is_array($name)) throw new \UnexpectedValueException('Node name expected as a string or array');

		$node =& $this->environments;
		$path = array();

		while($section = array_shift($name)) {
			// Shift current namespace section
			array_push($path, $section);
			if(!array_key_exists($section, $node)) throw new \UnexpectedValueException('Node '.implode('\\', $path).' missing');

			$pathNode =& $node[$section];

			if(is_object($pathNode) && method_exists($pathNode, 'node')) {
				// Pass request to child object
				return $pathNode->node($name);
			}

			if(is_array($pathNode)
				&& array_key_exists('_', $pathNode) && array_key_exists('self', $pathNode['_'])
				&& is_object($pathNode['_']['self']) && method_exists($pathNode['_']['self'], node)) {
					// Pass request to child node
					return $pathNode['_']['self']->node($name);
			}

			unset($node);
			$node =& $pathNode;
			unset($pathNode);
		}

		$this->wakeEnvironment($node);
		return $node;
	}

	/**
	 * __invoke specified environment component
	 * @param string $name Name of environment component
	 * @param array $argument Provided arguments
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		if(!array_key_exists($name, $this->environments)) throw new \BadMethodCallException("Environment component $name is not available.");
		if(is_object($this->environments[$name]) && method_exists($this->environments[$name], '__invoke'))
				// Pass call to node
				return call_user_func_array(array($this->environments[$name], '__invoke'), $arguments);
		return $this->environments[$name];
	}

}