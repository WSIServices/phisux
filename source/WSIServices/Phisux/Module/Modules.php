<?php

namespace WSIServices\Phisux\Module;
use \WSIServices\Common\DirectoryIncluder;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class Modules extends DirectoryIncluder {

	public $extension = '.cfg.php';

	protected $referanceSelf = true;

	protected $modules = array();

	public function __construct(&$configuration = null) {
		parent::__construct($configuration);

		// Add self to modules list
		if($this->referanceSelf) $this->modules['Modules'] =& $this;

		// Eager load modules
		if(key_exists('eagerLoad', $configuration)) $this->load($configuration['eagerLoad']);
	}

	/**
	 * Check if module is loadable
	 * @param string $name Name of module to check
	 * @param boolean $returnPath Return path of module if loadable
	 * @return mixed If $returnPath is true, return string of module path, else return boolean
	 */
	public function loadable($name, $returnPath = false) {
		return parent::loadable(strtolower($name), $returnPath);
	}

	/**
	 * Check if module is loaded
	 * @param string $name Name of module to check
	 * @return boolean true if module is loaded, false otherwise 
	 */
	public function loaded($name) {
		return key_exists($name, $this->modules);
	}

	/**
	 * Check if module is loaded or loadable
	 * @param string $name Name of module to check
	 * @return boolean true if module is loaded or loadable, false otherwise
	 */
	public function exists($name) {
		if($this->loaded($name)) return true;
		return $this->loadable($name);
	}

	/**
	 * Load specified module
	 * @param string $name Name of module
	 * @throws \BadMethodCallException
	 * @throws \InvalidArgumentException 
	 */
	public function load($name) {
		if(is_array($name)) foreach($name as $modLoad) $this->load($modLoad);
		elseif(is_string($name)) {
			try{
				$config = parent::load($name);
			} catch(UnexpectedValueException $e) {
				throw new \BadMethodCallException("Module $name is not available");
			}

			if(!is_array($config)) throw new \InvalidArgumentException("Module $name configuration is malformed");

			// Retrieve module 
			if(!key_exists('_construct', $config)) throw new \InvalidArgumentException("Module $name closure is missing from its configuration");
			$this->modules[$name] = $config['_construct']($config);			
			unset($config['_construct']);
		}
	}

	/**
	 * Retrieve specified module
	 * @param string $name Name of module to return
	 * @return mixed Requested module
	 */
	public function module($name) {
		// Load module if missing
		if(!key_exists($name, $this->modules)) $this->load($name);

		// Return module
		return $this->modules[$name];
	}

	/**
	 * Invoke specified module
	 * @param string $name Name of module
	 * @param array $arguments Arguments passed to module
	 * @return mixed Module or returned value from module __invode if exists 
	 */
	public function __call($name, $arguments) {

		$module = $this->module($name);

		// If module is invokeable call module with paramiters & return results
		if(is_object($module) && method_exists($module, '__invoke'))
			return call_user_func_array(array($module, '__invoke'), $arguments);

		// Return Module
		return $module;
	}

}