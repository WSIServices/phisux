<?php

namespace WSIServices\Phisux\Module\UrlHandler;
use \WSIServices\Common\DirectoryIncluder;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Create Object:
 *
 *	$configuration = array(
 *		'handler' => $urlHandler,
 *		'directory' => '/path/to/directory',
 *		'components' => array(
 *		),
 *	);
 *	$component = new Component($configuration);
 *
 */
class Component extends DirectoryIncluder {

	public $extension = '.cfg.php';

	/**
	 * @var UrlHandler 
	 */
	public $handler;

	/**
	 * @var array Sub Components for this component
	 */
	public $components;

	public function __call($name, $arguments) {
		
	}

}