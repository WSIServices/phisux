<?php

namespace WSIServices\Phisux\Module;
use \WSIServices\Common\DirectoryIncluder;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class UrlHandler extends DirectoryIncluder {

	const URL_SCHEME = 1;
	const URL_USER   = 2;
	const URL_PASS   = 4;
	const URL_HOST   = 8;
	const URL_PORT   = 16;
	const URL_PATH   = 32;
	const URL_QUERY  = 64;
	const URL_ALL    = 127;

	public $extension = '.cfg.php';

	public $request;

	public $handler = 'default';

	public $urlComponents = UrlHandler::URL_ALL;

	public $requestComponents = array();

	public $componentsPosition = 0;

	public function setRequestFromUrl($url) {
		if(!is_string($url))
			throw new \InvalidArgumentException('Provided argument should be a `string`, `'.gettype($url).'` provided.');

		$request = array();

		if(($this->urlComponents & UrlHandler::URL_SCHEME) === UrlHandler::URL_SCHEME)
			$request['scheme'] = parse_url($url, PHP_URL_SCHEME);
		if(($this->urlComponents & UrlHandler::URL_USER) === UrlHandler::URL_USER)
			$request['user'] = parse_url($url, PHP_URL_USER);
		if(($this->urlComponents & UrlHandler::URL_PASS) === UrlHandler::URL_PASS)
			$request['pass'] = parse_url($url, PHP_URL_PASS);
		if(($this->urlComponents & UrlHandler::URL_HOST) === UrlHandler::URL_HOST) {
			$request['host'] = parse_url($url, PHP_URL_HOST);
			if(is_string($request['host']))
				$request['host'] = explode('.', $request['host']);
		}
		if(($this->urlComponents & UrlHandler::URL_PORT) === UrlHandler::URL_PORT)
			$request['port'] = parse_url($url, PHP_URL_PORT);
		if(($this->urlComponents & UrlHandler::URL_PATH) === UrlHandler::URL_PATH) {
			$request['path'] = parse_url($url, PHP_URL_PATH);
			if(is_string($request['path']))
				$request['path'] = array_values(array_filter(explode('/', $request['path'])));
		}
		if(($this->urlComponents & UrlHandler::URL_QUERY) === UrlHandler::URL_QUERY) {
			$request['query'] = parse_url($url, PHP_URL_QUERY);
			if($request['query']) parse_str($request['query'], $request['query']);
		}

		$this->request = $request;
	}

	public function setRequestFromArray($requestArray) {
		$request = array();

		if(($this->urlComponents & UrlHandler::URL_SCHEME) === UrlHandler::URL_SCHEME) {
			if(key_exists('scheme', $requestArray) && $requestArray['scheme'])
				$request['scheme'] = $requestArray['scheme'];
			else $request['scheme'] = null;
		}
		if(($this->urlComponents & UrlHandler::URL_USER) === UrlHandler::URL_USER) {
			if(key_exists('user', $requestArray) && $requestArray['user'])
				$request['user'] = $requestArray['user'];
			else $request['user'] = null;
		}
		if(($this->urlComponents & UrlHandler::URL_PASS) === UrlHandler::URL_PASS) {
			if(key_exists('pass', $requestArray) && $requestArray['pass'])
				$request['pass'] = $requestArray['pass'];
			else $request['pass'] = null;
		}
		if(($this->urlComponents & UrlHandler::URL_HOST) === UrlHandler::URL_HOST) {
			if(key_exists('host', $requestArray)) {
				if(is_string($requestArray['host']))
					$request['host'] = explode('.', $requestArray['host']);
				elseif(is_array($requestArray['host']))
					$request['host'] = array_values(array_filter($requestArray['host']));
			} else $request['host'] = null;
		}
		if(($this->urlComponents & UrlHandler::URL_PORT) === UrlHandler::URL_PORT) {
			if(key_exists('port', $requestArray) && $requestArray['port'])
				$request['port'] = (int) $requestArray['port'];
			else $request['port'] = null;
		}
		if(($this->urlComponents & UrlHandler::URL_PATH) === UrlHandler::URL_PATH) {
			if(key_exists('path', $requestArray) && $requestArray['path']) {
				if(is_string($requestArray['path']))
					$requestArray['path'] = explode('/', $requestArray['path']);
				if(is_array($requestArray['path']))
					$request['path'] = array_values(array_filter($requestArray['path']));
			} else $request['path'] = null;
		}
		if(($this->urlComponents & UrlHandler::URL_QUERY) === UrlHandler::URL_QUERY) {
			if(key_exists('query', $requestArray) && $requestArray['query']) {
				$request['query'] = array();
				parse_str($requestArray['query'], $request['query']);
			} else $request['query'] = null;
		}

		$this->request = $request;
	}

	public function setRequestFromGlobals() {
		$requestArray = array();

		if(key_exists('SERVER_PROTOCOL', $_SERVER) && $_SERVER['SERVER_PROTOCOL'])
			$requestArray['scheme'] = strtolower(strstr($_SERVER['SERVER_PROTOCOL'], '/', true));
		if(key_exists('PHP_AUTH_USER', $_SERVER) && $_SERVER['PHP_AUTH_USER'])
			$requestArray['user'] = $_SERVER['PHP_AUTH_USER'];
		if(key_exists('PHP_AUTH_PW', $_SERVER) && $_SERVER['PHP_AUTH_PW'])
			$requestArray['pass'] = $_SERVER['PHP_AUTH_PW'];
		if(key_exists('HTTP_HOST', $_SERVER) && $_SERVER['HTTP_HOST'])
			$requestArray['host'] = $_SERVER['HTTP_HOST'];
		if(key_exists('SERVER_PORT', $_SERVER) && $_SERVER['SERVER_PORT'])
			$requestArray['port'] = (int) $_SERVER['SERVER_PORT'];
		if(key_exists('REQUEST_URI', $_SERVER) && $_SERVER['REQUEST_URI']) {
			$requestArray['path'] = strstr($_SERVER['REQUEST_URI'], '?', true);
			if($requestArray['path'] === false) $requestArray['path'] = $_SERVER['REQUEST_URI'];
		}
		if(key_exists('QUERY_STRING', $_SERVER) && $_SERVER['QUERY_STRING'])
			$requestArray['query'] = $_SERVER['QUERY_STRING'];

		$this->setRequestFromArray($requestArray);
	}

	public function setRequest($url = null) {
		if(is_string($url)) {
			$this->setRequestFromUrl($url);
		} elseif(is_array($url)) {
			$this->setRequestFromArray($url);
		} else {
			$this->setRequestFromGlobals();
		}
		return;
	}

	public function callRequest($url = null) {
		if($url !== null) $this->setRequest($url);
	}
}
