<?php

namespace WSIServices\Phisux\Module;
use \WSIServices\Common\DirectoryIncluder;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class Environments extends DirectoryIncluder {

	public $extension = '.cfg.php';

	/**
	 * @var string Selected default environment class
	 */
	public $environmentClass = 'WSIServices\Phisux\Module\Environments\Environment';

	/**
	 * @var string Selected default environment
	 */
	protected $environment = 'default';

	/**
	 * @var array Loaded environments
	 */
	protected $environments = array();

	/**
	 * Provide configuration
	 * @param mixed $configuration [optional]
	 *
	 * @example Providing No Variable
	 *	$environments = new Environments();
	 *
	 * @example Providing Empty Variable
	 *	$environments = new Environments($configuration);
	 *
	 * @example Providing Empty Variable
	 *	$configuration = __DIR__;
	 *	$environments = new Environments($configuration);
	 *
	 * @example Providing Array
	 *	$configuration = array(
	 *		'directory' => __DIR__,
	 *		'extension' => '.php',
	 *	);
	 *	$environments = new Environments($configuration);
	 *
	 */
	public function __construct(&$configuration = null) {
		parent::__construct($configuration);
	}

	/**
	 * Determin if specified environment is loaded
	 * @param string $name Name of environment
	 * @return boolean 
	 */
	public function isLoaded($name) {
		return key_exists($name, $this->environments);
	}

	/**
	 * Determine if specified environment is loaded or loadable
	 * @param string $name Name of environment
	 * @return boolean 
	 */
	public function exists($name) {
		return ($this->isLoaded($name) || $this->loadable($name));
	}

	/**
	 * Set or get current default environment
	 * @param string $name [optional] Name of environment to set as default
	 * @return string Name of environment set as default
	 * @throws \UnexpectedValueException 
	 */
	public function defaultEnvironment($name = null) {
		if($name === null) return $this->environment;
		if(!$this->exists($name))
			throw new \UnexpectedValueException("Specified environment $name does not exist");

		$this->environment = $name;
	}

	/**
	 * Set specified environment
	 * @param string $name Name of environment
	 * @param mixed $environment Environment object or array
	 * @param boolean $default [optional] Set environment as default
	 * @throws \UnexpectedValueException
	 * @throws \InvalidArgumentException 
	 */
	public function set($name, &$environment, $default = false) {
		if($this->isLoaded($name))
			throw new \UnexpectedValueException("Specified environment $name is already loaded");
		if(!(is_array($environment) || is_object($environment)))
			throw new \InvalidArgumentException("Provided envirnoment is not an array or object");
		
		$this->environments[$name] =& $environment;
		if($default) $this->environment = $name;
	}

	/**
	 * Unset specified environment
	 * @param type $name
	 * @throws \UnexpectedValueException 
	 */
	public function remove($name) {
		if(!$this->isLoaded($name))
			throw new \UnexpectedValueException("Specified environment $name is not loaded");
		unset($this->environments[$name]);
	}

	/**
	 * Load specified environment file
	 * @param string $name Environment to load
	 * @param boolean $default [optional] Set as default environment
	 * @return boolean 
	 */
	public function load($name, $default = false) {
		try {
			$configuration = parent::load($name);
		} catch(\UnexpectedValueException $e) {
			return false;
		}

		$this->set($name, $configuration, $default);
		return true;
	}

	protected function wakeEnvironment(&$environment) {
		if(is_object($environment)) return true;

		if(!is_array($environment))	return false;

		if(key_exists('_', $environment) && key_exists('type', $environment['_'])) {
			$environmentClass = $environment['_']['type'];
		} else {
			$environmentClass = $this->environmentClass;
		}

		$newEnv = new $environmentClass($environment);
		$environment = $newEnv;

		return true;
	}

	/**
	 * Get specified environment
	 * @param string $name [optional] Name of environment to return
	 * @return mixed 
	 * @throws \UnexpectedValueException 
	 */
	public function &environment($name = null) {
		if($name === null) $name = $this->environment;

		if(!($this->isLoaded($name) || $this->load($name)))
			throw new \UnexpectedValueException("Requested environmet $name does not exist");

		if(!$this->wakeEnvironment($this->environments[$name]))
			throw new \UnexpectedValueException("Requested envirnoment $name is not a compond type (object or array.)");

		return $this->environments[$name];
	}

}