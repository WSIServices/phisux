<?php

namespace WSIServices\Phisux\Module;
use \WSIServices\Common\ArrayClass;

/**
 * @package Phisux
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * Fork from WSI Services Core AutoLoad, Last updated in 2009
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class AutoLoad extends ArrayClass {

	protected $handlers = array();

	protected $splAttached = true;
	protected $extractOnLoad = true;
	protected $extractOnAttach = true;
	protected $injectOnDetach = true;

	public $loadStack = array();
	public $currentLoad = null;

	public function __construct(&$configuration = null) {
		parent::__construct($configuration);

		// Initialize and register AutoLoad
		if($this->splAttached) {
			$this->splAttached = false;
			$this->splAttach();
		}
	}

	public function __destruct() {
		if($this->splAttached) {
			$this->splDetach();
			$this->splAttached = true;
		}
	}

	/**
	 * Register Self with SPL AutoLoad
	 */
	public function splAttach() {
		if($this->extractOnAttach) $this->splExtract();
		if(!$this->splAttached) spl_autoload_register(array($this, 'load'), true, true);
		$this->splAttached = true;
	}

	/**
	 * Unregister Self from SPL AutoLoad 
	 */
	public function splDetach() {
		if($this->injectOnDetach) $this->splInject();
		spl_autoload_unregister(array($this, 'load'));
		$this->splAttached = false;
	}

	/**
	 * Unregister SPL AutoLoad functions and register with self in namespace '\'
	 */
	protected function splExtract() {
		// Current registered SPL autoload handlers
		$autoloadHandlers = spl_autoload_functions();

		// Unregister current SPL autoload handlers & register with AutoLoad
		if($autoloadHandlers) {
			$thisCallback = array($this, 'load');
			foreach($autoloadHandlers as $SPLautoload) {
				if($SPLautoload !== $thisCallback) {
					$this->register($SPLautoload);
					spl_autoload_unregister($SPLautoload);
				}
			}
		}
	}

	/**
	 * Register all handlers directly with SPL AutoLoad
	 */
	protected function splInject() {
		foreach($this->handlers as $namespace => $handlers) {
			foreach($handlers as $hPos => $handler) {
				spl_autoload_register($handler);
				unset($this->handlers[$namespace][$hPos]);
			}
			unset($this->handlers[$namespace]);
		}
	}

	/**
	 * Register a callback to the AutoLoad handler
	 * @param callback $callback A callback to locate, include, and return value 
	 * @param mixed $namespace [optional] A string or an array of strings of valid namespeces
	 * @throws UnexpectedValueException
	 */
	public function register($callback, $namespace = null) {
		// Can't allow registation of self
        if($callback === array($this, 'load') || !is_callable($callback, true))
            throw new UnexpectedValueException('The callback value provided is invalid');

		if($namespace === null) $namespace = array('');
		elseif(is_string($namespace)) $namespace = explode('\\',$namespace);

		if(!is_array($namespace))
			throw new UnexpectedValueException('Namespace value provided is invalid, expects a single or an array of valid namespace strings');

		foreach ($namespace as $ns) {
			if(!is_string($ns) || $ns === '') $ns = '\\';
			if($ns[0] !== '\\') $ns = '\\'.$ns;

			if(!key_exists($ns, $this->handlers)) $this->handlers[$ns] = array();
			$this->handlers[$ns][] = $callback;
		}
	}

	/**
	 * Unregister a callback from the AutoLoad handler
	 * @param callback $callback
	 * @throws UnexpectedValueException 
	 */
	public function unregister($callback) {
        if(!is_callable($callback, true))
            throw new UnexpectedValueException('The callback value provided is invalid');

		foreach($this->handlers as $namespace => $callbacks) {
			foreach ($callbacks as $key => $cb) {
				if($callback === $cb) unset($this->handlers[$namespace][$key]);
			}
		}
	}

	/**
	 * Try all registered handlers to load the requested class
	 * @param string $class
	 * @param type $return
	 * @return type 
	 */
	public function load($class, &$return = null) {
		// If currently in a recursive load, push previous cycle to stack
		if($this->currentLoad !== null) array_push($this->loadStack, $this->currentLoad);

		// Initialize AutoLoad
		elseif($this->extractOnLoad) $this->splExtract();

		$this->currentLoad = $class;

		// Clean class with namespace
		$namespace = array_filter(explode('\\', $this->currentLoad));
		array_unshift($namespace, '');

		$remainingClass = array();
		$return = false;

		// Loop through handler namespaces and mach to class namespace
		while(count($namespace)) {
			$ns = implode('\\', $namespace);
			if($ns === '') $ns = '\\';

			if(key_exists($ns, $this->handlers)) {
				foreach($this->handlers[$ns] as $handler) {
					$return = call_user_func($handler, $this->currentLoad);
					if($return !== false) break 2;
				}
			}

			// Pull least important namespace/class name
			array_unshift($remainingClass, array_pop($namespace));
		}

		// If currently in a recursive load pop previous cycle out of stack
		if(count($this->loadStack)) $this->currentLoad = array_pop($this->loadStack);
		else $this->currentLoad = null;
		
		return (bool) $return;
	}

}