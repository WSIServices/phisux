<?php
/**
 * Degrees of Success Site Resources
 * 
 * @author Sam Likins
 * @package Resource
 */

namespace Resource;

/**
 * Tonic Resource for Home
 * @uri /
 * @priority 50
 */
class Home extends \WSIServices\TonicComponents\Resource {

	/**
	 * Handles GET Requests
	 * @routeMethod get 
	 * @return \Tonic\Response 
	 */
	public function getLogic() {
		return $this->displayHome();
	}

	/**
	 * Processes template returning Response
	 * @return \Tonic\Response 
	 */
	public function displayHome() {
		return $this->renderTemplate('Page/Home.twig');
	}

}