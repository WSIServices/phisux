<?php

return array(
	'_construct' => function(&$config) { return new WSIServices\Common\PageData($config); },
	'section' => 'main',
	'values' => array(
		'header' => '',
	),
	'headers' => array(
		'title' => 'Degrees of Success',
		'keywords' => array(),
		'description' => '',
		'cssLink' => array(
			'normalize' => array(
				'href' => '/css/normalize.css',
				'media' => 'all',
			),
			'main' => array(
				'href' => '/css/main.css',
				'media' => 'all',
			),
		),
		'css' => array(),
		'javascriptLink' => array(
			'modernizr' => '/js/vendor/modernizr-2.6.2.min.js',
			'jquery' => '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
			'plugins' => '/js/plugins.js',
			'main' => '/js/main.js',
		),
		'javascript' => array(
			'window.jQuery || document.write(\'<script src="/js/vendor/jquery-1.9.1.min.js"><\/script>\')'
		),
	),
);