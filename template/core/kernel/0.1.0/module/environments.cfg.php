<?php

return array(
	'_construct' => function(&$config) { return new WSIServices\Phisux\Module\Environments($config); },
	'directory' => \WSIServices\Phisux\Kernel::$environmentDirectory,
);