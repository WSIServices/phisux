<?php

use WSIServices\Phisux\Kernel,
	Tonic\Application;

return array(
	'_construct' => function(&$config) {
		$directory = Kernel::Environments()->path('tonic');

		if($directory) {
			Kernel::getAutoLoader()->add('Resource', $directory);
			$directory .= \DIRECTORY_SEPARATOR.'Resource';
			$files = \DIRECTORY_SEPARATOR.'*.php';

			$config['load'] = array(
				$directory.$files,
//				$directory.\DIRECTORY_SEPARATOR.'directoryname'.$files,
			);
			
		}

		return new Application($config);
	},
);