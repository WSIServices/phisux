<?php

use WSIServices\Phisux\Kernel,
	Doctrine\Common\ClassLoader,
	Doctrine\ORM\Configuration,
	Doctrine\Common\Cache,
	Doctrine\ORM\Mapping\Driver\YamlDriver,
	Doctrine\ORM\EntityManager,
	Doctrine\DBAL\Types\Type;

return array(
	'connectionOptions' => array(
		'driver' => 'pdo_mysql',
		'host' => 'localhost',
		'dbname' => '{databasename}',
		'user' => '{username}',
		'password' => '{password}',
	),
	'proxyNamespace' => '{sitename}\Site\Proxy',
	'proxiesDirectory' => 'Proxies',
	'yamlDirectory' => array(
		\WSIServices\Phisux\Kernel::$vendorDirectory.'/{backend-projectname}/Database/Doctrine/Yaml',
	),
	'_construct' => function(&$config) {
		// Proxy configuration
		$envDirectory = Kernel::Environments()->path('doctrine');

		if($envDirectory) {
			$proxiesDirectory = $envDirectory.\DIRECTORY_SEPARATOR.$config['proxiesDirectory'];
		}

		if($proxiesDirectory) {
			// Set up class loading.
			$classLoader = new ClassLoader('Proxies', $proxiesDirectory);
			$classLoader->register();
		}


		// Doctrine Configuration
		$configuration = new Configuration;

		if($proxiesDirectory) {
			$configuration->setProxyDir($proxiesDirectory);
			$configuration->setProxyNamespace($config['proxyNamespace']);
		}

		if(defined('APP_ENVIRONMENT') && APP_ENVIRONMENT === 'PRODUCTION') {
			$configuration->setAutoGenerateProxyClasses(false);
			$cache = new Cache\ApcCache;
		} else {
			$configuration->setAutoGenerateProxyClasses(true);
			$cache = new Cache\ArrayCache;
		}

		$configuration->setMetadataCacheImpl($cache);
		$configuration->setQueryCacheImpl($cache);

		$driverImpl = new YamlDriver($config['yamlDirectory']);
//		$driverImpl->setFileExtension('.yml');
		$configuration->setMetadataDriverImpl($driverImpl);


		// Create EntityManager
		$em = EntityManager::create($config['connectionOptions'], $configuration);


		// Define ENUM data type
		$platform = $em->getConnection()->getDatabasePlatform();
		$platform->registerDoctrineTypeMapping('enum', 'string');

		return $em;
	},
);
