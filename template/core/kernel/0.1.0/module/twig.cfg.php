<?php

use WSIServices\Phisux\Kernel;

return array(
	'_construct' => function(&$config) {
		$directory = Kernel::Environments()->path('twig');
		if($directory) {
			$config['cache'] = $directory.\DIRECTORY_SEPARATOR.'Cache';
			$twigLoader = new Twig_Loader_Filesystem(
				$directory.\DIRECTORY_SEPARATOR.'Templates'
			);
		} else {
			$twigLoader = new Twig_Loader_String();
		}

		$twig = new Twig_Environment($twigLoader, $config);
		$twig->addGlobal('Kernel', Kernel::getInstance());
		$twig->addGlobal('PageData', Kernel::pageData());
//		$twig->addGlobal('ControlManager', Kernel::ControlManager());
		$twig->addGlobal('_SERVER', $_SERVER);

		return $twig;
	},
	'debug' => true,
);
