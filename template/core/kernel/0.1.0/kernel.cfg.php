<?php

use WSIServices\Phisux;

return array(
	'coreDirectory' => CORE_DIR,
	'vendorDirectory' => VENDOR_DIR,
	'environmentDirectory' => ENVIRONMENT_DIR,
	'temporaryDirectory' => TEMPORARY_DIR,

	// Start Kernel Section: Initialization of Kernel and system
	'start_kernel' => array(
		// Eager Load Section: array of class files
		'eagerLoad' => array(
			'WSIServices\Common\ArrayClass',
			'WSIServices\Common\DirectoryIncluder',
			'WSIServices\Phisux\Module\Modules',
		),
		'modulesHandler' => array(
			'_construct' => function(&$config) { return new Phisux\Module\Modules($config); },
			'directory' => KERNEL_DIR.DIRECTORY_SEPARATOR.'module',
			'eagerLoad' => array(
				'ClosureHelper',
				'Environments',
			),
		),
		// Initialization Section: array of closures
		// Closure provided contents of this config array as first paramiter
		'init' => array(
//			'Application' => function() {
//				Phisux\Application::initializeEnvironment(Phisux\Kernel::environment());
//			},
		),
	),
);
