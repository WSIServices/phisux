<?php

ini_set('display_errors', 'stdout'); // stderr or stdout
error_reporting(E_ALL);

define('CORE_DIR', __DIR__);

define('KERNELS_DIR', CORE_DIR.DIRECTORY_SEPARATOR.'kernel');
define('ENVIRONMENT_DIR', CORE_DIR.DIRECTORY_SEPARATOR.'env');
define('VENDOR_DIR', dirname(dirname(CORE_DIR)).DIRECTORY_SEPARATOR.'vendor');
define('TEMPORARY_DIR', CORE_DIR.DIRECTORY_SEPARATOR.'tmp');

// If KERNEL_OVERRIDE is defined in calling script, active kernel can be changed
// WARNING: An E_COMPILE_ERROR will be produced if defined kernel does not exist
if(defined('KERNEL_OVERRIDE')) {
	define('KERNEL_DIR', KERNELS_DIR.DIRECTORY_SEPARATOR.KERNEL_OVERRIDE);
} else {
	define('KERNEL_DIR', KERNELS_DIR.DIRECTORY_SEPARATOR.'0.1.0');
}


/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

// Kernel Wiring

use WSIServices\Phisux;

// Include Vendor Autoload
$vendorAutoLoad = require VENDOR_DIR.DIRECTORY_SEPARATOR.'autoload.php';

Phisux\Kernel::setAutoLoader($vendorAutoLoad, 'loadClass');

Phisux\Kernel::start_kernel(include KERNEL_DIR.DIRECTORY_SEPARATOR.'kernel.cfg.php');

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */