<?php

use WSIServices\Phisux\Kernel;

if(!defined('CORE_DIR')) require '../core/bootloader.php';

$kernel = Kernel::getInstance();

try {
	$resource = $kernel->TonicApplication()->getResource(new Tonic\Request);

	$resource->setContainer($kernel); // attach container to loaded resource
	$resource->setCallable('renderTemplate', function($template) {
		return new Tonic\Response(
			Tonic\Response::OK,
			$this->container->Twig()->render($template)
		);
	});

	$response = $resource->exec();

} catch (Tonic\NotFoundException $e) {
	$response = new Tonic\Response(
		404,
		$kernel->Twig()->render('Error/404.twig')
	);

} catch (Tonic\UnauthorizedException $e) {
	$response = new Tonic\Response(
		401,
		$kernel->Twig()->render('Error/401.twig')
	);
	$response->wwwAuthenticate = 'Basic realm="Protected Zone"';

} catch (Tonic\Exception $e) {
	$response = new Tonic\Response(
		500,
		$kernel->Twig()->render('Error/500.twig')
	);

}

$response->output();